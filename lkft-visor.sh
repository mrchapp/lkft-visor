#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
VISOR_CONFIG="${HOME}/.config/lkft-visor.conf"

## First time setup
if [ ! -f "${VISOR_CONFIG}" ]; then
  "${ROOT_DIR}/setup-visor.sh"
fi
set -a
# shellcheck source=/dev/null
. "${VISOR_CONFIG}"
set +a

. "${ROOT_DIR}/lib.sh"
set -e

## Peco setup
if ! command -v peco >/dev/null 2>&1; then
  echo "Please install \`peco'."
  echo "  apt install peco"
  exit 1
fi

peco_config="${HOME}/.config/peco/config.json"
if [ ! -f "${peco_config}" ]; then
  mkdir -p "$(dirname "${peco_config}")"
  cp -p "${ROOT_DIR}/peco-config.json.dist" "${peco_config}"
fi

## Local data store
mkdir -p "${VISOR_DATA_DIR}"
mkdir -p "${VISOR_CACHE_DOWNLOADS}"

echo "Fetching LORE for stable mailing list..."
clone_or_update \
  https://lore.kernel.org/stable/1 \
  origin1 \
  "${VISOR_LORE_DIR}" \
  master

clone_or_update \
  https://github.com/Linaro/lkft-tools.git \
  origin \
  "${VISOR_DATA_DIR}/lkft-tools" \
  master

#echo "Fetching Linux Stable RC Git tree..."
#clone_or_update \
#  https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git \
#  linux-stable-rc \
#  /data/linux-stable-rc \
#  master

echo "Entering LKFT visor..."
"${ROOT_DIR}/view-main.sh" | peco --prompt "LKFT>" --exec "${ROOT_DIR}/view-build.sh"
