#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

set -o pipefail
set -e
kernelversion="$1"
test_id="$2"

read -r line || true
#echo "line: [${line}]"
cmd="$(echo "${line}" | grep -Eo '\[V:.*\]' | tr -d '[]' || true)"
#echo "cmd:  [${cmd}]"
subcmd="$(echo "${cmd}" | cut -d: -f5 || true)"

if [ -z "${kernelversion}" ]; then
  kernelversion="$(echo "${cmd}" | cut -d: -f6 || true)"
fi

if [ -z "${test_id}" ]; then
  test_id="$(echo "${cmd}" | cut -d: -f7 || true)"
fi

if [ -z "${kernelversion}" ] || [ -z "${test_id}" ]; then
  echo "No kernel version or no test ID. Can't continue."
  exit 0
fi

case "${subcmd}" in
  L)
    test_url="$(get_squad_api)/tests/${test_id}/"
    test_json="$(squad_download_and_cache "${test_url}")"

    testrun_url="$(jq -r ".test_run" "${test_json}")"
    testrun_json="$(download_and_cache "${testrun_url}")"

    logfile_url="$(jq -r ".log_file" "${testrun_json}")"
    logfile="$(download_and_cache "${logfile_url}")"

    if [ -s "${logfile}" ]; then
      less -R "${logfile}"
    fi
    ;;
  W)
    test_url="$(get_squad_api)/tests/${test_id}/"
    test_json="$(squad_download_and_cache "${test_url}")"

    testrun_url="$(jq -r ".test_run" "${test_json}")"
    testrun_json="$(download_and_cache "${testrun_url}")"

    job_url="$(jq -r ".job_url" "${testrun_json}")"
    xdg-open "${job_url}"
    ;;
esac
