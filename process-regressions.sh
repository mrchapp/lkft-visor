#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

read -r line
#echo "line: [${line}]"
cmd="$(echo "${line}" | grep -Eo '\[V:.*\]' | tr -d '[]')"
#echo "cmd:  [${cmd}]"

subcmd="$(echo "${cmd}" | cut -d: -f4)"
case "${subcmd}" in
  H)
    kernelversion="$(echo "${cmd}" | cut -d: -f5)"
    arch_regression="$(echo "${cmd}" | cut -d: -f6)"

    if [ "${kernelversion:0:5}" = "next-" ]; then
      branch="linux-next"
    elif [ "${kernelversion:0:2}" = "v6" ]; then
      branch="mainline"
    else
      branch="$(get_branch_from_makefile_version "${kernelversion}")"
    fi
    slug="$(get_squad_project_slug_by_branch "${branch}")"

    regression="$(echo "${arch_regression}" | cut -d/ -f2-)"
    xdg-open "${SQUAD_SERVER}/${SQUAD_GROUP}/${slug}/tests/${regression}"
    ;;
  I)
    kernelversion="$(echo "${cmd}" | cut -d: -f5)"
    regression="$(echo "${cmd}" | cut -d: -f6)"
    branch="$(get_branch_from_makefile_version "${kernelversion}")"
    dest="${VISOR_DATA_DIR}/user/regressions/${branch}"
    mkdir -p "$(dirname "${dest}")"
    echo "${regression}:${kernelversion}" >> "${dest}"
    ;;
esac
