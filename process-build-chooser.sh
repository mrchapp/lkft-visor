#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

read -r line
#echo "line: [${line}]"
cmd="$(echo "${line}" | grep -Eo '\[V:.*\]' | tr -d '[]')"
#echo "cmd:  [${cmd}]"

subcmd="$(echo "${cmd}" | cut -d: -f4)"
case "${subcmd}" in
  S)
    kernelversion="$(echo "${cmd}" | cut -d: -f5)"
    new_baseline="$(echo "${cmd}" | cut -d: -f6)"
    dest="${VISOR_DATA_DIR}/user/builds/${kernelversion}"
    mkdir -p "$(dirname "${dest}")"
    echo "${new_baseline}" > "${dest}"
    ;;
esac
