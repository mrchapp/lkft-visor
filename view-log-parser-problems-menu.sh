#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

set -o pipefail
set -e
kernelversion="$1"
test_id="$2"

if [ -z "${kernelversion}" ] || [ -z "${test_id}" ]; then
  echo "No kernel version or no test ID. Can't continue."
  exit 0
fi

peco_prompt="LKFT / ${kernelversion} / ${test_id}>"
(
echo "  Kernel version: ${kernelversion}"
echo "  test_id:        ${test_id}"

if [ "${kernelversion:0:5}" = "next-" ]; then
  build_id="${kernelversion}"
  build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
elif [ "${kernelversion:0:2}" = "v6" ]; then
  build_id="${kernelversion}"
  build_url="$(get_build_url_for_build mainline "${kernelversion}")"
else
  build_id="$(get_build_id_for_kernelversion "${kernelversion}")"
  build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
fi
test_url="${build_url}/tests/${test_id}/"
test_json="$(squad_download_and_cache "${test_url}")"

env_url="$(jq -r .environment "${test_json}")"
env_slug="$(squad_environment_to_string "${env_url}")"

testrun_url="$(jq -r .test_run "${test_json}")"
testrun_json="$(squad_download_and_cache "${testrun_url}")"
tests_url="$(jq -r .tests "${testrun_json}")"
tests_json="$(squad_download_and_cache "${tests_url}")"


echo "  Environment:    ${env_slug}"
echo "  Log:"
jq -r .log "${test_json}" | awk '{print "    " $0}'
echo "  Related tests:"
jq -r '.results[] | .result,.name' "${tests_json}" | pr -aTt -2 -o4 -s"	" | sort -k2
echo "---"
echo_cmd "V:B:F:M:L:${kernelversion}:${test_id}" "    View test log"
echo_cmd "V:B:F:M:W:${kernelversion}:${test_id}" "    View test data (on browser)"

) | peco --prompt "${peco_prompt}" --exec "${ROOT_DIR}/process-log-parser-problems-menu.sh"
