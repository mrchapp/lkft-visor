#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

list_of_builds_txt="$(mktemp)"
"${VISOR_DATA_DIR}/lkft-tools/bin/build_info.py" "${SQUAD_SERVER}/${SQUAD_GROUP}/linux-next-master/" | head -n5 > "${list_of_builds_txt}"
"${VISOR_DATA_DIR}/lkft-tools/bin/build_info.py" "${SQUAD_SERVER}/${SQUAD_GROUP}/linux-mainline-master/" | head -n5 >> "${list_of_builds_txt}"
"${VISOR_DATA_DIR}/lkft-tools/bin/lore-helper.py" -C "${VISOR_LORE_DIR}" -d "${VISOR_LORE_DAYS}" list-rcs >> "${list_of_builds_txt}"

while read -r line; do
  datetime_utc="$(echo "${line}" | cut -d, -f1)"
  moniker="$(echo "${line}" | cut -d, -f2)"
  rc="$(echo "${line}" | cut -d, -f3)"
  status="$(echo "${line}" | cut -d, -f4)"
  sla="$(echo "${line}" | cut -d, -f5)"

  # if [ "${status}" == "Replied" ]; then
  #   continue
  # fi

  datetime_utc_ts="$(TZ=UTC date +%s -d "${datetime_utc}")"
  ago="$(hdate "@${datetime_utc_ts}")"

  if [ "${moniker}" = "stable" ]; then
    branch="$(get_branch_from_makefile_version "${rc}")"
    url="$(get_squad_project_url_by_branch "${branch}")"
    git_describe="$(get_build_id_for_kernelversion "${rc}")"
    build_url="$(get_build_url_for_kernelversion "${rc}")"
  else
    url="$(get_squad_project_url_by_branch "${moniker}")"
    branch="master"
    git_describe="${rc}"
    build_url="$(get_build_url_for_build "${moniker}" "${rc}")"
  fi
  builds_json="$(download_and_cache "${url}/builds")"

  metadata_url="${build_url}/metadata"

  # If build has no results yet
  if [ -z "${git_describe}" ]; then
    num_jobs_complete=0
    num_jobs_total=0
  else
    # Get completion percentage
    summary_json="$(download_and_cache "${build_url}/testjobs_summary/")"
    jobs_complete="$(jq -r .results.Complete "${summary_json}" || echo 0)"
    jobs_incomplete="$(jq -r .results.Incomplete "${summary_json}" || echo 0)"
    jobs_null="$(jq -r .results.null "${summary_json}" || echo 0)"
    jobs_submitted="$(jq -r .results.Submitted "${summary_json}" || echo 0)"
    num_jobs_complete=$((jobs_complete + jobs_incomplete))
    num_jobs_total=$((jobs_complete + jobs_incomplete + jobs_null + jobs_submitted))
  fi

  pct_jobs_complete=0
  bars_jobs_complete=0
  if [ "${num_jobs_total}" -gt 0 ]; then
    pct_jobs_complete="$(echo "scale=2; ${num_jobs_complete} / ${num_jobs_total} * 100" | bc | cut -d. -f1)"
    # complete bars out of 10
    bars_jobs_complete="$(echo "scale=2; (5 + (${num_jobs_complete} / ${num_jobs_total} * 100)) / 10" | bc | cut -d. -f1)"
  fi
  bars_jobs_pending=$((10 - bars_jobs_complete))
  bars_string="$(printf "%${bars_jobs_complete}s" | tr ' ' '#')$(printf "%${bars_jobs_pending}s" | tr ' ' '-')"

  #jq -r . "${builds_json}"
  pad_rc="$(printf "%-32s" "${rc}")"
  pad_id="$(printf "%-32s" "${git_describe}")"
  pad_jobs_complete="$(printf "%4d" "${num_jobs_complete}")"
  pad_jobs_total="$(printf "%4d" "${num_jobs_total}")"
  pad_pct="$(printf "%3d" "${pct_jobs_complete}")"

  pad_status="$(printf "%-14s" "${status}")"
  if [ ! "${sla}" == "None" ]; then
    ago="${ago} (${sla})"
  fi
  echo -e "${pad_rc} \t ${pad_id} \t [${bars_string}] ${pad_jobs_complete}/${pad_jobs_total} ${pad_pct}%   ${pad_status} ${ago}"
done < <(sort -ur "${list_of_builds_txt}")
echo "---"
