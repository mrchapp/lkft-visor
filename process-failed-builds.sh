#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

read -r line
#echo "line: [${line}]"
cmd="$(echo "${line}" | grep -Eo '\[V:.*\]' | tr -d '[]')"
#echo "cmd:  [${cmd}]"

subcmd="$(echo "${cmd}" | cut -d: -f4)"
case "${subcmd}" in
  B)
    kernelversion="$(echo "${cmd}" | cut -d: -f5)"
    test_id="$(echo "${cmd}" | cut -d: -f6)"

    if [ "${kernelversion:0:5}" = "next-" ]; then
      build_id="${kernelversion}"
      build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
    elif [ "${kernelversion:0:2}" = "v6" ]; then
      build_id="${kernelversion}"
      build_url="$(get_build_url_for_build mainline "${kernelversion}")"
    else
      build_id="$(get_build_id_for_kernelversion "${kernelversion}")"
      build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
    fi
    failed_builds_url="${build_url}/tests/?metadata__suite=build&result=false"
    failed_builds_json="$(download_and_cache "${failed_builds_url}")"
    testrun_url="$(jq -r ".results[] | select(.id == ${test_id}) | .test_run" "${failed_builds_json}")"
    testrun_json="$(download_and_cache "${testrun_url}")"
    logfile_url="$(jq -r ".log_file" "${testrun_json}")"
    #job_url="$(jq -r ".job_url" "${testrun_json}")"

    logfile="$(download_and_cache "${logfile_url}")"
    if [ -s "${logfile}" ]; then
      less -R "${logfile}"
    fi
    ;;
esac
