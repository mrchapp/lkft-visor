#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

read -r line
#echo "line: [${line}]"
cmd="$(echo "${line}" | grep -Eo '\[V:.*\]' | tr -d '[]')"
#echo "cmd:  [${cmd}]"

subcmd="$(echo "${cmd}" | cut -d: -f4)"
case "${subcmd}" in
  M)
    kernelversion="$(echo "${cmd}" | cut -d: -f5)"
    test_id="$(echo "${cmd}" | cut -d: -f6)"
    "${ROOT_DIR}/view-log-parser-problems-menu.sh" "${kernelversion}" "${test_id}"
    ;;
esac
