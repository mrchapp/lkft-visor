#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

kernelversion="$1"

peco_prompt="LKFT / ${kernelversion} / Baseline>"

if [ "${kernelversion:0:5}" = "next-" ]; then
  build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
  branch="linux-next"
  git_describe="${kernelversion}"
  baseline_id="$(get_baseline_id_for_build linux-next "${kernelversion}")"
elif [ "${kernelversion:0:2}" = "v6" ]; then
  build_url="$(get_build_url_for_build mainline "${kernelversion}")"
  branch="mainline"
  git_describe="${kernelversion}"
  baseline_id="$(get_baseline_id_for_build mainline "${kernelversion}")"
else
  build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
  branch="$(get_branch_from_makefile_version "${kernelversion}")"
  git_describe="$(get_build_id_for_kernelversion "${kernelversion}")"
  baseline_id="$(get_baseline_id_for_kernelversion "${kernelversion}")"
fi
url="$(get_squad_project_url_by_branch "${branch}")"
builds_json="$(download_and_cache "${url}/builds")"

(
echo "  Build:    ${git_describe}"
echo "  Baseline: ${baseline_id}"

while read -r buildline; do
  version="$(echo "${buildline}" | cut -d, -f1)"
  star=" "
  if [ "${version}" == "${baseline_id}" ]; then
    star="*"
  fi
  full_datetime="$(echo "${buildline}" | cut -d, -f2)"
  datetime="$(date -d "${full_datetime}" +"%Y-%m-%d %H:%M:%S")"
  datetime_ago="$(hdate "${datetime}")"
  content="$(printf "    %19s  %-32s  %15s" "${datetime}" "${version} ${star}" "${datetime_ago}")"
  echo_cmd "V:B:B:S:${kernelversion}:${version}" "${content}"
done < <(jq -r '.results[] | .version + "," + .datetime' "${builds_json}")
) | peco --prompt "${peco_prompt}" --exec "${ROOT_DIR}/process-build-baseline.sh"
