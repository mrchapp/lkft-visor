#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

rc="$1"
if [ -z "${rc}" ]; then
  read -r line
  rc="$(echo "${line}" | awk '{print $1}')"
  #build_id="$(echo "${line}" | awk '{print $2}')"
fi
if [ "${rc:0:5}" = "next-" ]; then
  git_describe="${rc}"
  build_url="$(get_build_url_for_build linux-next "${rc}")"
  branch="linux-next"
  baseline_id="$(get_baseline_id_for_build linux-next "${rc}")"
elif [ "${rc:0:2}" = "v6" ]; then
  git_describe="${rc}"
  build_url="$(get_build_url_for_build mainline "${rc}")"
  branch="mainline"
  baseline_id="$(get_baseline_id_for_build mainline "${rc}")"
else
  git_describe="$(get_build_id_for_kernelversion "${rc}")"
  build_url="$(get_build_url_for_kernelversion "${rc}")"
  branch="$(get_branch_from_makefile_version "${rc}")"
  baseline_id="$(get_baseline_id_for_kernelversion "${rc}")"
fi

status_json="$(download_and_cache "${build_url}/status/")"
summary_json="$(download_and_cache "${build_url}/testjobs_summary/")"

tests_total="$(jq -r ".tests_total" "${status_json}")"
tests_pass="$(jq -r ".tests_pass" "${status_json}")"
tests_fail="$(jq -r ".tests_fail" "${status_json}")"
tests_skip="$(jq -r ".tests_skip" "${status_json}")"
tests_xfail="$(jq -r ".tests_xfail" "${status_json}")"

testruns_total="$(jq -r ".test_runs_total" "${status_json}" | sed -s 's:null:0:')"
testruns_complete="$(jq -r ".test_runs_completed" "${status_json}" | sed -s 's:null:0:')"
testruns_incomplete="$(jq -r ".test_runs_incomplete" "${status_json}" | sed -s 's:null:0:')"
#testruns_pending="$((testruns_total - (testruns_complete + testruns_incomplete)))"
bar_length=80
# The "* 10 + 5 / 10" part is rounding craziness
bar_tr_complete=0
bar_tr_incomplete=0
if [ "${testruns_total}" -gt 0 ]; then
  bar_tr_complete=$((((bar_length * 10 * testruns_complete / testruns_total) + 5) / 10))
  bar_tr_incomplete=$((((bar_length * 10 * testruns_incomplete / testruns_total) + 5) / 10))
fi
bar_tr_pending=$((bar_length - (bar_tr_complete + bar_tr_incomplete)))
bar_tr_string="$(printf "%${bar_tr_complete}s" | tr ' ' '#')$(printf "%${bar_tr_incomplete}s" | tr ' ' '!')$(printf "%${bar_tr_pending}s" | tr ' ' '-')"

testjobs_complete="$(jq -r ".results.Complete" "${summary_json}" | sed -s 's:null:0:')"
testjobs_incomplete="$(jq -r ".results.Incomplete" "${summary_json}" | sed -s 's:null:0:')"
testjobs_pending="$(jq -r ".results.null" "${summary_json}" | sed -s 's:null:0:')"
testjobs_total=$((testjobs_complete + testjobs_incomplete + testjobs_pending))
bar_length=80
# The "* 10 + 5 / 10" part is rounding craziness
bar_tj_complete=0
bar_tj_incomplete=0
if [ "${testjobs_total}" -gt 0 ]; then
  bar_tj_complete=$((((bar_length * 10 * testjobs_complete / testjobs_total) + 5) / 10))
  bar_tj_incomplete=$((((bar_length * 10 * testjobs_incomplete / testjobs_total) + 5) / 10))
fi
bar_tj_pending=$((bar_length - (bar_tj_complete + bar_tj_incomplete)))
bar_tj_string="$(printf "%${bar_tj_complete}s" | tr ' ' '#')$(printf "%${bar_tj_incomplete}s" | tr ' ' '!')$(printf "%${bar_tj_pending}s" | tr ' ' '-')"

failed_builds_url="${build_url}/tests/?metadata__suite=build&result=false"
failed_builds_json="$(download_and_cache "${failed_builds_url}")"
total_failed_builds="$(jq -r .results[].short_name "${failed_builds_json}" | wc -l)"
total_new_warnings="$(get_metrics_regressions "${branch}" "${git_describe}" "${baseline_id}" | wc -l)"

(
echo "${line}"
echo "  Tests:    ${tests_total} total, ${tests_pass}/${tests_skip}/${tests_fail}/${tests_xfail} (pass/skip/fail/xfail)"
echo "  Testjobs: [${bar_tj_string}] ${testjobs_complete}+${testjobs_incomplete} / ${testjobs_total}"
echo "  Testruns: [${bar_tr_string}] ${testruns_complete}+${testruns_incomplete} / ${testruns_total}"
content="$(printf "  Build:    %-20s" "${git_describe}")"
if [ "${rc:0:5}" = "next-" ]; then
  echo "${content}"
else
  content="${content}       (select to change build)"
  echo_cmd "V:B:C:${rc}" "${content}"
fi
content="$(printf "  Baseline: %-20s       (select to change baseline)" "${baseline_id}")"
echo_cmd "V:B:B:${rc}" "${content}"
content="$(printf "  Failed builds: %3d    (select to view detail)" "${total_failed_builds}")"
echo_cmd "V:B:F:${rc}" "${content}"
content="$(printf "  New warnings: %3d     (select to view detail)" "${total_new_warnings}")"
echo_cmd "V:B:W:${rc}" "${content}"
if [ -f "${VISOR_DATA_DIR}/reports/report-${rc}.txt" ]; then
  report_datetime_ts="$(stat -c %Y "${VISOR_DATA_DIR}/reports/report-${rc}.txt")"
  report_ago="$(hdate "@${report_datetime_ts}")"
  content="$(printf "%-20s" "${report_ago}")"
  echo_cmd "V:B:R:${rc}" "  Report: Created ${content}       (select to regenerate report)"
  # Regressions
  if [ -f "${VISOR_DATA_DIR}/reports/regressions-${rc}.txt" ]; then
    list_regressions="$(mktemp)"
    ignore_list="${VISOR_DATA_DIR}/user/regressions/${branch}"

    sort "${VISOR_DATA_DIR}/reports/regressions-${rc}.txt" | grep ^Reg | awk '{print $2}' > "${list_regressions}"

    regressions_ignored=0
    if [ -s "${ignore_list}" ]; then
      regressions_total="$(grep -c -v -f <(awk -F: '{print $1}' "${ignore_list}") "${list_regressions}")"
      regressions_ignored="$(wc -l "${ignore_list}" | awk '{print $1}')"
    else
      regressions_total="$(wc -l "${list_regressions}" | awk '{print $1}')"
    fi

    if [ "${regressions_total}" -gt 0 ]; then
      reg_msg="${regressions_total} suspected regressions"
    else
      reg_msg="No statistical regressions"
    fi

    if [ "${regressions_ignored}" -gt 0 ]; then
      reg_msg="${reg_msg} (${regressions_ignored} ignored)"
    fi

    echo "    ${reg_msg}"
    while read -r regression; do
      if grep -q "^${regression}" "${ignore_list}" 2>/dev/null; then
        continue
      fi
      echo_cmd "V:B:I:${rc}" "      ${regression}"
    done < "${list_regressions}"
  fi
  # Kernel panics
  if [ -f "${VISOR_DATA_DIR}/reports/log-parser-test-${rc}.txt.gz" ]; then
    list_kernel_panics="$(mktemp)"
    zgrep "fail$" "${VISOR_DATA_DIR}/reports/log-parser-test-${rc}.txt.gz" | grep -e panic -e exception| cut -d/ -f1-2 | sort -u > "${list_kernel_panics}"

    kernel_panic_total="$(wc -l "${list_kernel_panics}" | awk '{print $1}')"
    if [ "${kernel_panic_total}" -gt 0 ]; then
      kp_msg="${kernel_panic_total} suspected kernel panics/exceptions"
    else
      kp_msg="No kernel panics/exceptions"
    fi

    echo_cmd "V:B:L:${rc}" "    ${kp_msg}"
    while read -r kernel_panic; do
      #echo_cmd "V:B:P:${rc}" "      ${kernel_panic}"
      echo "      ${kernel_panic}"
    done < "${list_kernel_panics}"
  fi
  echo_cmd "V:B:V:${rc}" "    View report"
  echo_cmd "V:B:G:${rc}" "    Force clean report (no regressions, no fixes)"
  echo_cmd "V:B:E:${rc}" "    Edit report"
  echo_cmd "V:B:M:${rc}" "    Send email reply"
else
  echo_cmd "V:B:R:${rc}" "    No report yet     (select to generate report)"
fi
) | peco --prompt "LKFT / ${rc}>" --exec "${ROOT_DIR}/process-build.sh"
