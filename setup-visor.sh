#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
VISOR_CONFIG="${HOME}/.config/lkft-visor.conf"

. "${ROOT_DIR}/lib.sh"

# $1: question
# $2: variable to define
# $3: default value
ask_and_set() {
  question="$1"
  setting="$2"
  default="$3"

  if [ -v "${setting}" ] && [ -n "${!setting}" ]; then
    default="${!setting}"
  fi
  set +x

  echo -n "${question} [${default}]> "
  read -r answer

  if [ -z "${answer}" ]; then
    answer="${default}"
  fi

  replace_line "${VISOR_CONFIG}" "${setting}=" "${answer}"
}

if [ ! -f "${VISOR_CONFIG}" ]; then
  echo "File ${VISOR_CONFIG} does not exist."
  echo "Assuming this is a first-run."
else
  set -a
  # shellcheck source=/dev/null
  . "${VISOR_CONFIG}"
  set +a
fi

mkdir -p "$(dirname "${VISOR_CONFIG}")"
echo "Now configuring LKFT Visor (${VISOR_CONFIG})."
echo
ask_and_set "Where should clones and state files be saved?" "VISOR_DATA_DIR" "\${HOME}/.local/lkft-visor}"
ask_and_set "How long should caches live for (in minutes)?" "VISOR_CACHE_EXPIRATION" "30"
ask_and_set "Wher should cached files be stored?" "VISOR_CACHE_DOWNLOADS" "\${HOME}/.local/lkft-visor/downloads"
ask_and_set "Where should a copy of LORE live (+1 GB of storage)?" "VISOR_LORE_DIR" "/data/lore-stable"
ask_and_set "How many days to look back in the mail for RC's?" "VISOR_LORE_DAYS" "7"
ask_and_set "SQUAD token to use (required)?" "SQUAD_TOKEN" ""
