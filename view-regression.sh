#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

kernelversion="$1"
regression="$2"

peco_prompt="LKFT / ${kernelversion} / ${regression}>"
(
echo "  Kernel version: ${kernelversion}"
echo "  Regression:     ${regression}"
echo_cmd "V:B:I:H:${kernelversion}:${regression}" "    View history (on browser)"
echo_cmd "V:B:I:I:${kernelversion}:${regression}" "    Mark as ignored"
) | peco --prompt "${peco_prompt}" --exec "${ROOT_DIR}/process-regressions.sh"
