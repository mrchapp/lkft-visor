#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

kernelversion="$1"

peco_prompt="LKFT / ${kernelversion} / Log parser problems>"

if [ "${kernelversion:0:5}" = "next-" ]; then
  build_id="${kernelversion}"
  build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
elif [ "${kernelversion:0:2}" = "v6" ]; then
  build_id="${kernelversion}"
  build_url="$(get_build_url_for_build mainline "${kernelversion}")"
else
  build_id="$(get_build_id_for_kernelversion "${kernelversion}")"
  build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
fi

failed_builds_url="${build_url}/tests/?metadata__suite__in=log-parser-test,log-parser-boot&result=false"
failed_builds_json="$(squad_download_and_cache "${failed_builds_url}")"
#cp -p "${failed_builds_json}" "/tmp/logs.json"
(
echo "  Kernel version: ${kernelversion}"
echo "  Build:          ${build_id}"

(
while read -r test_id; do
  short_name="$(jq -r ".results[] | select(.id == ${test_id}) | .short_name" "${failed_builds_json}")"
  environment_url="$(jq -r ".results[] | select(.id == ${test_id}) | .environment" "${failed_builds_json}")"
  #echo "${short_name}:     url: ${environment_url}"
  environment_json="$(download_and_cache "${environment_url}")"
  environment="$(jq -r ".slug" "${environment_json}")"
  content="$(printf "    %-35s %-20s\n" "${short_name}" "${environment}")"
  echo_cmd "V:B:F:M:${kernelversion}:${test_id}" "${content}"
  #echo "${content}"
done < <(jq -r '.results[].id' "${failed_builds_json}")
) | sort -V
) | peco --prompt "${peco_prompt}" --exec "${ROOT_DIR}/process-log-parser-problems.sh"
