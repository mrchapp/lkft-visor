#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

if [ $# -lt 4 ]; then
  echo "Need branch, kernelversion, build and baseline as arguments. E.g.:"
  echo "  $0 5.4 v5.4.11-rc2 v5.4.10-20-g123456 v5.4.10"
  exit 1
fi

squad_report_clean=""
if [ "$1" == "--clean" ]; then
  squad_report_clean="--clean"
  shift
fi

export BRANCH="$1"
export KERNELVERSION="$2"
export BUILD="$3"
export BASELINE="$4"
project_slug="$(get_squad_slug_by_branch "${BRANCH}")"
export project_slug

cat << EOF
#!/bin/bash

. "${ROOT_DIR}/lib.sh"

clone_or_update \
  https://gitlab.com/Linaro/lkft/reports/squad-report.git \
  origin \
  "${VISOR_DATA_DIR}/squad-report" \
  master

clone_or_update \
  https://github.com/Linaro/squad-client.git \
  origin \
  "${VISOR_DATA_DIR}/squad-client" \
  master

squad_report_config_yml="\$(download_and_cache https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/squad-report-config.yml)"
sed -i -e '/output:/d' "\${squad_report_config_yml}"

export BASELINE="${BASELINE}"
export BUILD="${BUILD}"

echo "- Sanity report..."
"${VISOR_DATA_DIR}/squad-report/squad-report" \\
  --url="${SQUAD_SERVER}" \\
  --group="${SQUAD_GROUP}" \\
  --config "\${squad_report_config_yml}" \\
  --config-report-type=build \\
  --unfinished \\
  --project="${project_slug}-sanity" \\
  --base-build \${BASELINE} \\
  --build \${BUILD} \\
  ${squad_report_clean[@]} \\
  > "${VISOR_DATA_DIR}/reports/report-${KERNELVERSION}-sanity.txt"

echo "- Complete report..."
timeout 5m "${VISOR_DATA_DIR}/squad-report/squad-report" \\
  --url="${SQUAD_SERVER}" \\
  --group="${SQUAD_GROUP}" \\
  --config "\${squad_report_config_yml}" \\
  --config-report-type=report \\
  --unfinished \\
  --project="${project_slug}" \\
  --base-build \${BASELINE} \\
  --build \${BUILD} \\
  ${squad_report_clean[@]} \\
  > "${VISOR_DATA_DIR}/reports/report-${KERNELVERSION}.txt"

echo "- Complete regressions..."
timeout 5m "${VISOR_DATA_DIR}/squad-report/scripts/squad-find-regressions" \\
  --group="${SQUAD_GROUP}" \\
  --project="${project_slug}" \\
  --build \${BUILD} \\
  > "${VISOR_DATA_DIR}/reports/regressions-${KERNELVERSION}.txt"

echo "- Test log parser..."
pushd "${VISOR_DATA_DIR}/squad-client" > /dev/null
python3 -m squad_client.manage \\
  --squad-host "${SQUAD_SERVER}" \\
  download-results \\
  --group="${SQUAD_GROUP}" \\
  --project="${project_slug}" \\
  --build \${BUILD} \\
  --suites log-parser-test,log-parser-boot \\
  --format "{test.environment.slug}/{test.test_run.metadata.build_name}/{test.name} {test.status}" \\
  --filename "${VISOR_DATA_DIR}/reports/log-parser-test-${KERNELVERSION}.txt"
gzip -f9 "${VISOR_DATA_DIR}/reports/log-parser-test-${KERNELVERSION}.txt"
popd > /dev/null
EOF
