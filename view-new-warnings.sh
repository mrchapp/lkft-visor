#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

kernelversion="$1"

peco_prompt="LKFT / ${kernelversion} / New warnings>"

if [ "${kernelversion:0:5}" = "next-" ]; then
  build_id="${kernelversion}"
  build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
  branch="linux-next"
  baseline_id="$(get_baseline_id_for_build linux-next "${kernelversion}")"
elif [ "${kernelversion:0:2}" = "v6" ]; then
  build_id="${kernelversion}"
  build_url="$(get_build_url_for_build mainline "${kernelversion}")"
  branch="mainline"
  baseline_id="$(get_baseline_id_for_build mainline "${kernelversion}")"
else
  build_id="$(get_build_id_for_kernelversion "${kernelversion}")"
  build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
  branch="$(get_branch_from_makefile_version "${kernelversion}")"
  baseline_id="$(get_baseline_id_for_kernelversion "${kernelversion}")"
fi

warnings_list="$(mktemp)"
# Subshell as to preserve local variables:
(get_metrics_regressions "${branch}" "${build_id}" "${baseline_id}" > "${warnings_list}")

(
echo "  Kernel version: ${kernelversion}"
echo "  Build:          ${build_id}"
echo "  Baseline:       ${baseline_id}"

(
while read -r arch_kconfig; do
  arch="$(echo "${arch_kconfig}" | cut -d/ -f1)"
  kconfig="$(echo "${arch_kconfig}" | cut -d/ -f2)"

  content="$(printf "    %-45s %-20s\n" "${kconfig}" "${arch}")"
  #echo_cmd "V:B:W:B:${kernelversion}:${test_id}" "${content}"
  echo "${content}"
done < "${warnings_list}"
) | sort -V
) | peco --prompt "${peco_prompt}" --exec "${ROOT_DIR}/process-failed-builds.sh"
