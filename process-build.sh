#!/bin/bash

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
. "${ROOT_DIR}/lib.sh"

read -r line
#echo "line: [${line}]"
cmd="$(echo "${line}" | grep -Eo '\[V:.*\]' | tr -d '[]')"
#echo "cmd:  [${cmd}]"

subsubcmd="$(echo "${cmd}" | cut -d: -f3)"
case "${subsubcmd}" in
  B)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    "${ROOT_DIR}/view-build-baseline.sh" "${kernelversion}"
    ;;
  C)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    "${ROOT_DIR}/view-build-chooser.sh" "${kernelversion}"
    ;;
  E)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    xdg-open "${VISOR_DATA_DIR}/reports/report-${kernelversion}.txt"
    ;;
  F)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    "${ROOT_DIR}/view-failed-builds.sh" "${kernelversion}"
    ;;
  L)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    "${ROOT_DIR}/view-log-parser-problems.sh" "${kernelversion}"
    ;;
  G)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"

    if [ "${kernelversion:0:5}" = "next-" ]; then
      build_id="${kernelversion}"
      build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
      branch="linux-next"
      baseline_id="$(get_baseline_id_for_build linux-next "${kernelversion}")"
    elif [ "${kernelversion:0:2}" = "v6" ]; then
      build_id="${kernelversion}"
      build_url="$(get_build_url_for_build mainline "${kernelversion}")"
      branch="mainline"
      baseline_id="$(get_baseline_id_for_build mainline "${kernelversion}")"
    else
      build_id="$(get_build_id_for_kernelversion "${kernelversion}")"
      build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
      branch="$(get_branch_from_makefile_version "${kernelversion}")"
      baseline_id="$(get_baseline_id_for_kernelversion "${kernelversion}")"
    fi

    reporter="${VISOR_DATA_DIR}/reports/gen-report-${branch}.sh"
    mkdir -p "$(dirname "${reporter}")"
    "${ROOT_DIR}/create-reporter.sh" --clean "${branch}" "${kernelversion}" "${build_id}" "${baseline_id}" > "${reporter}"
    chmod +x "${reporter}"
    echo "Generating (clean) report for ${kernelversion}..."
    bash "${reporter}"
    ;;
  I)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    regression="$(echo "${line}" | awk '{print $1}')"
    "${ROOT_DIR}/view-regression.sh" "${kernelversion}" "${regression}"
    ;;
  M)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    in_reply_to="$("${VISOR_DATA_DIR}/lkft-tools/bin/lore-helper.py" -C "${VISOR_LORE_DIR}" -d 10 -g "${kernelversion}" show)"
    reply_email_eml="$(mktemp --suffix=".eml")"
    "${VISOR_DATA_DIR}/lkft-tools/bin/lore-helper.py" \
      -C "${VISOR_LORE_DIR}" \
      -i "${in_reply_to}" \
      --report "${VISOR_DATA_DIR}/reports/report-${kernelversion}.txt" \
      --reply-to=all reply \
      > "${reply_email_eml}"
    xdg-open "${reply_email_eml}"
    ;;
  R)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"

    if [ "${kernelversion:0:5}" = "next-" ]; then
      build_id="${kernelversion}"
      build_url="$(get_build_url_for_build linux-next "${kernelversion}")"
      branch="linux-next"
      baseline_id="$(get_baseline_id_for_build linux-next "${kernelversion}")"
    elif [ "${kernelversion:0:2}" = "v6" ]; then
      build_id="${kernelversion}"
      build_url="$(get_build_url_for_build mainline "${kernelversion}")"
      branch="mainline"
      baseline_id="$(get_baseline_id_for_build mainline "${kernelversion}")"
    else
      build_id="$(get_build_id_for_kernelversion "${kernelversion}")"
      build_url="$(get_build_url_for_kernelversion "${kernelversion}")"
      branch="$(get_branch_from_makefile_version "${kernelversion}")"
      baseline_id="$(get_baseline_id_for_kernelversion "${kernelversion}")"
    fi

    reporter="${VISOR_DATA_DIR}/reports/gen-report-${branch}.sh"
    mkdir -p "$(dirname "${reporter}")"
    "${ROOT_DIR}/create-reporter.sh" "${branch}" "${kernelversion}" "${build_id}" "${baseline_id}" > "${reporter}"
    chmod +x "${reporter}"
    echo "Generating report for ${kernelversion}..."
    bash "${reporter}"
    ;;
  V)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    less "${VISOR_DATA_DIR}/reports/report-${kernelversion}.txt"
    ;;
  W)
    kernelversion="$(echo "${cmd}" | cut -d: -f4)"
    "${ROOT_DIR}/view-new-warnings.sh" "${kernelversion}"
    ;;
esac
